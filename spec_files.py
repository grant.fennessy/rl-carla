import os
from carla_env.env import ENV_CONFIG
import carla_env.termination as terminations
import carla_env.scenarios as scenarios
import deepq_learner


# Filenames
from utils.deeplab import DeepLabModel
from utils.spec_file_reader import DEEPQ_CONFIG_BASE

_FOLDER = "/media/grant/FastData/carla"
FILENAMES = {
    "carla": _FOLDER,
    "checkpoints": os.path.join(_FOLDER, "checkpoints"),
    "model": os.path.join(_FOLDER, "model.pkl"),
    "results": os.path.join(_FOLDER, "results.csv"),
    "summary": os.path.join(_FOLDER, "summary.yaml"),
    "crashes": os.path.join(_FOLDER, "crashes.csv"),
    "out-of-lanes": os.path.join(_FOLDER, "out-of-lanes.csv"),
    "deeplab_graph": "/media/grant/FastData/deeplab/export/frozen_inference_graph.pb"
}

# Make folders
if not os.path.exists(FILENAMES["carla"]):
    os.mkdir(FILENAMES["carla"])
if not os.path.exists(FILENAMES["checkpoints"]):
    os.mkdir(FILENAMES["checkpoints"])

RESOLUTION = 160

######################################################################
ENV_BASE = ENV_CONFIG.copy()
ENV_BASE.update({
    "verbose": False,
    "carla_out_path": FILENAMES["carla"],
    "save_images_rgb": False,
    "save_images_class": False,
    "save_images_fusion": False,
    "save_image_frequency": 10,
    "convert_images_to_video": False,
    "render_x_res": RESOLUTION,
    "render_y_res": RESOLUTION,
    "x_res": RESOLUTION,
    "y_res": RESOLUTION,
    "fps": 50,
    "deeplab": None,
    "quality": "Low",
    "sensor_style": "GROUND_TRUTH",
    "use_depth_camera": False,
    "server_map": "/Game/Maps/Town02",
    "reward_function": "train_1",
    "enable_planner": False,
    "use_dsf": True,
    "framestack": 2,
    terminations.EARLY_TERMINATIONS: [
        terminations.TERMINATE_ON_COLLISION,
        terminations.TERMINATE_ON_OFFROAD,
        # terminations.TERMINATE_ON_OTHERLANE,
        terminations.TERMINATE_NO_MOVEMENT,
        # terminations.TERMINATE_NOT_PERFECT
    ],
    "scenarios": scenarios.TOWN2_TRAIN,
    "reward_noise": 0.0,
    "episode_deadzone": 100,
    "control_gamma": 0.75
})

ENV_LEVELS = {
    "test": ENV_BASE.copy(),
    "test_traintown": ENV_BASE.copy(),
    "record": ENV_BASE.copy(),
    "base": ENV_BASE,
    "train_1": ENV_BASE.copy(),
    "train_2": ENV_BASE.copy(),
    "train_3": ENV_BASE.copy(),
}
ENV_LEVELS['test'].update({
    "scenarios": scenarios.TOWN1_EVAL,
    "server_map": "/Game/Maps/Town01",
    "deterministic_scenario_count": 1,
    "save_images_rgb": True,
    "save_images_class": True,
    "save_images_fusion": True,
    "save_image_raw_class": True,
    "save_ds_components": True,
    "save_image_frequency": 1,
})
ENV_LEVELS['test_traintown'].update({
    "scenarios": scenarios.TOWN2_EVAL,
    "server_map": "/Game/Maps/Town02",
    "deterministic_scenario_count": 1
})
ENV_LEVELS['record'].update({
    "reward_noise": 0.0,
    "quality": "Epic",
    "save_images_rgb": True,
    "save_images_class": True,
    "save_images_fusion": True,
    "save_image_frequency": 1,

    # Test map
    # "scenarios": scenarios.TOWN1_EVAL,
    # "server_map": "/Game/Maps/Town01",

    # Train map
    "scenarios": scenarios.TOWN2_EVAL,
    "server_map": "/Game/Maps/Town02",
})


def use_deeplab(env):
    env.update({
        "deeplab": DeepLabModel(FILENAMES["deeplab_graph"]),  # Use model
        "sensor_style": "DEEPLAB",
        "quality": "Epic",
    })
def use_deeplab_mixed(env):
    env.update({
        "deeplab": DeepLabModel(FILENAMES["deeplab_graph"]),  # Use model
        "sensor_style": "RANDOM",
        "quality": "Epic",
    })

######################################################################
__TRAIN_FREQ = 6

DEEPQ_BASE = DEEPQ_CONFIG_BASE.copy()
DEEPQ_BASE.update({
    "checkpoint_path": FILENAMES["checkpoints"],
    "episode_deadzone": ENV_BASE["episode_deadzone"]
})

DEEPQ_BASE = {
    "test": DEEPQ_BASE.copy(),
    "record": DEEPQ_BASE.copy(),
    "base": DEEPQ_BASE,
    "train_1": DEEPQ_BASE.copy(),
    "train_2": DEEPQ_BASE.copy(),
    "train_3": DEEPQ_BASE.copy(),
}

DEEPQ_BASE['train_1'].update({
    "lr": 1e-4,
    "max_timesteps": int(1e6),
    "exploration_blocks": {
        "change_frequency": 200,
        "strategies": [
            [0.10, 0.60],
            [0.20, 0.45],
            [0.40, 0.30],
            [0.20, 0.15],
            [0.10, 0.00]  # Yields an average of 30% exploration
        ]
    },
    # "exploration_start_eps": 1.0,
    # "exploration_fraction": 0.1,
    # "exploration_final_eps": 0.1
})
DEEPQ_BASE['train_2'].update({
    "lr": 1e-5,
    "train_freq": 18,
    "learning_starts": 500,
    "max_timesteps": int(2e6),
    "exploration_blocks": {
        "change_frequency": 200,
        "strategies": [
            [0.10, 0.20],
            [0.10, 0.15],
            [0.20, 0.10],
            [0.40, 0.05],
            [0.20, 0.00]  # Yields an average of 7.5% exploration
        ]
    },
    # "exploration_start_eps": 0.1,
    # "exploration_fraction": 0.5,
    # "exploration_final_eps": 0.05,
})
DEEPQ_BASE['train_3'].update({
    "lr": 1e-6,
    "train_freq": 18,
    "learning_starts": 5000,
    "max_timesteps": int(3e6),
    "exploration_blocks": {
        "change_frequency": 200,
        "strategies": [
            [0.10, 0.30],
            [0.10, 0.20],
            [0.20, 0.10],
            [0.60, 0.00]  # Yields an average of 5% exploration
        ]
    }
})
# TODO -> test and record should be running eval on the models and not need train stats
DEEPQ_BASE['test'].update({
    "lr": 1e-10,
    "max_timesteps": int(1e6),
    "buffer_size": int(100),
    "exploration_start_eps": 0.0,
    "train_freq": int(2e6),
    "learning_starts": int(2e6),
    "checkpoint_freq": int(2e6),
})
DEEPQ_BASE['record'].update({
    "lr": 1e-10,
    "max_timesteps": int(1e6),
    "buffer_size": int(1e3),
    "exploration_fraction": 0.0001,
    "exploration_final_eps": 0.0001,
    "train_freq": 40000000,
    "learning_starts": 1000000,
    "checkpoint_freq": 10000000,
})