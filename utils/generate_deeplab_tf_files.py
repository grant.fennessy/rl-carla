
import collections
import six
import sys
import os
import math
from utils.seg_images import SegmentationFilenamesQueue
import tensorflow as tf


class ImageReader(object):
  """Helper class that provides TensorFlow image coding utilities."""

  def __init__(self, image_format='jpeg', channels=3):
    """Class constructor.
    Args:
      image_format: Image format. Only 'jpeg', 'jpg', or 'png' are supported.
      channels: Image channels.
    """
    with tf.Graph().as_default():
      self._decode_data = tf.placeholder(dtype=tf.string)
      self._image_format = image_format
      self._session = tf.Session()
      if self._image_format in ('jpeg', 'jpg'):
        self._decode = tf.image.decode_jpeg(self._decode_data,
                                            channels=channels)
      elif self._image_format == 'png':
        self._decode = tf.image.decode_png(self._decode_data,
                                           channels=channels)

  def read_image_dims(self, image_data):
    """Reads the image dimensions.
    Args:
      image_data: string of image data.
    Returns:
      image_height and image_width.
    """
    image = self.decode_image(image_data)
    return image.shape[:2]

  def decode_image(self, image_data):
    """Decodes the image data string.
    Args:
      image_data: string of image data.
    Returns:
      Decoded image data.
    Raises:
      ValueError: Value of image channels not supported.
    """
    image = self._session.run(self._decode,
                              feed_dict={self._decode_data: image_data})
    if len(image.shape) != 3 or image.shape[2] not in (1, 3):
      raise ValueError('The image channels not supported.')

    return image


def _int64_list_feature(values):
  """Returns a TF-Feature of int64_list.
  Args:
    values: A scalar or list of values.
  Returns:
    A TF-Feature.
  """
  if not isinstance(values, collections.Iterable):
    values = [values]

  return tf.train.Feature(int64_list=tf.train.Int64List(value=values))



def _bytes_list_feature(values):
  """Returns a TF-Feature of bytes.
  Args:
    values: A string.
  Returns:
    A TF-Feature.
  """
  def norm2bytes(value):
    return value.encode() if isinstance(value, str) and six.PY3 else value

  return tf.train.Feature(
      bytes_list=tf.train.BytesList(value=[norm2bytes(values)]))


def image_seg_to_tfexample(image_data, seg_data, filename, height=160, width=160):
  """Converts one image/segmentation pair to tf example.
  Args:
    image_data: string of image data.
    filename: image filename.
    height: image height.
    width: image width.
    seg_data: string of semantic segmentation data.
  Returns:
    tf example of one image/segmentation pair.
  """
  return tf.train.Example(features=tf.train.Features(feature={
      'image/encoded': _bytes_list_feature(image_data),
      'image/filename': _bytes_list_feature(filename),
      'image/format': _bytes_list_feature('png'),
      'image/height': _int64_list_feature(height),
      'image/width': _int64_list_feature(width),
      'image/channels': _int64_list_feature(3),
      'image/segmentation/class/encoded': (
          _bytes_list_feature(seg_data)),
      'image/segmentation/class/format': _bytes_list_feature('png'),
  }))







SPECS = {
    'train': {
        'shards': 20,
        'src_folder': '/media/grant/FastData/deeplab/segmentation/train/',
        'out_folder': '/media/grant/FastData/deeplab/data',
        'out_name': 'train'
    },
    'test': {
        'shards': 5,
        'src_folder': '/media/grant/FastData/deeplab/segmentation/test/',
        'out_folder': '/media/grant/FastData/deeplab/data',
        'out_name': 'val'
    }
}



def _convert_dataset(dset_name):
  """Converts the specified dataset split to TFRecord format.
  Args:
    dataset_split: The dataset split (e.g., train, test).
  Raises:
    RuntimeError: If loaded image and label have different shape.
  """
  sys.stdout.write('Processing ' + dset_name)
  specs = SPECS[dset_name]
  queue = SegmentationFilenamesQueue(specs['src_folder'])
  shards = specs['shards']
  out_folder = specs['out_folder']
  out_name = specs['out_name']

  num_images = queue.count()
  num_per_shard = int(math.ceil(num_images / float(shards)))

  for shard_id in range(shards):
    output_filename = os.path.join(out_folder, '%s-%05d-of-%05d.tfrecord' % (out_name, shard_id, shards))
    with tf.python_io.TFRecordWriter(output_filename) as tfrecord_writer:
      start_idx = shard_id * num_per_shard
      end_idx = min((shard_id + 1) * num_per_shard, num_images)
      for i in range(start_idx, end_idx):
        sys.stdout.write('\r>> Converting image {index:>08}/{total} shard {shard}'.format(
            index=i+1,
            total=num_images,
            shard=shard_id
        ))
        sys.stdout.flush()

        # Read the image and semantic segmentation
        image_filename, seg_filename= queue.next()
        image_data = tf.gfile.FastGFile(image_filename, 'rb').read()
        seg_data = tf.gfile.FastGFile(seg_filename, 'rb').read()

        # Convert to tf example.
        example = image_seg_to_tfexample(image_data, seg_data, image_filename)
        tfrecord_writer.write(example.SerializeToString())

    sys.stdout.write('\n')
    sys.stdout.flush()


def main(unused_argv):
    _convert_dataset('train')
    _convert_dataset('test')


if __name__ == '__main__':
  tf.app.run()
