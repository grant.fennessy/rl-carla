import tensorflow as tf
import tensorflow.contrib.layers as layers


def model_function(is_training=False):
    return lambda *args, **kwargs: build_drive_model(is_training=is_training, *args, **kwargs)


def convolve(layer, num_outputs, kernel_size, stride, activation_fn=tf.nn.relu, padding="SAME"):
    return layers.convolution2d(layer,
                                num_outputs=num_outputs,
                                kernel_size=kernel_size,
                                stride=stride,
                                activation_fn=activation_fn,
                                padding=padding)


def max_pool(layer, kernel_size=[2, 2], stride=2, padding="SAME"):
    return layers.max_pool2d(layer,
                             kernel_size=kernel_size,
                             stride=stride,
                             padding=padding)


def build_drive_model(inpt, num_actions, scope, reuse=False, is_training=False):
    with tf.variable_scope(scope, reuse=reuse):
        out = inpt

        # Convolutional layers
        # http://fomoro.com/tools/receptive-fields/index.html#3,2,1,SAME;3,2,1,SAME;3,2,1,SAME;3,1,1,SAME;3,1,1,SAME;3,1,1,SAME;2,2,1,SAME
        with tf.variable_scope("convnet"):
            conv_out = max_pool(out)

            conv_out = convolve(conv_out, num_outputs=64, kernel_size=3, stride=1)
            conv_out = convolve(conv_out, num_outputs=64, kernel_size=3, stride=1)
            conv_out = convolve(conv_out, num_outputs=64, kernel_size=3, stride=1)
            conv_out = max_pool(conv_out)

            conv_out = convolve(conv_out, num_outputs=128, kernel_size=3, stride=1)
            conv_out = convolve(conv_out, num_outputs=128, kernel_size=3, stride=1)
            conv_out = convolve(conv_out, num_outputs=128, kernel_size=3, stride=1)
            conv_out = max_pool(conv_out)

            conv_out = convolve(conv_out, num_outputs=256, kernel_size=3, stride=1)
            conv_out = convolve(conv_out, num_outputs=256, kernel_size=3, stride=1)
            conv_out = convolve(conv_out, num_outputs=256, kernel_size=3, stride=1)
            conv_out = max_pool(conv_out)
            conv_out = layers.flatten(conv_out)

        # Deep Layers
        with tf.variable_scope("action_value"):
            action_out = conv_out
            action_out = layers.dropout(action_out, keep_prob=0.7, is_training=is_training)
            action_out = layers.fully_connected(action_out, num_outputs=2048, activation_fn=tf.nn.relu)
            action_out = layers.dropout(action_out, keep_prob=0.7, is_training=is_training)
            action_out = layers.fully_connected(action_out, num_outputs=2048, activation_fn=tf.nn.relu)
            action_out = layers.fully_connected(action_out, num_outputs=num_actions, activation_fn=None)

        # Dueling
        with tf.variable_scope("state_value"):
            state_out = conv_out
            state_out = layers.dropout(state_out, keep_prob=0.7, is_training=is_training)
            state_out = layers.fully_connected(state_out, num_outputs=2048, activation_fn=tf.nn.relu)
            state_out = layers.dropout(state_out, keep_prob=0.7, is_training=is_training)
            state_out = layers.fully_connected(state_out, num_outputs=2048, activation_fn=tf.nn.relu)
            state_out = layers.fully_connected(state_out, num_outputs=num_actions, activation_fn=None)

            action_scores_mean = tf.reduce_mean(action_out, 1)
            action_scores_centered = action_out - tf.expand_dims(action_scores_mean, 1)
            q_out = state_out + action_scores_centered

    return q_out
