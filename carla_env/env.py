"""OpenAI gym environment for Carla. Run this file for a demo."""

from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

from datetime import datetime
import atexit
import os
import json
import random
import signal
import subprocess
import time
import traceback

import numpy as np

from carla_env import scenarios
from done_error import DoneError

try:
    import scipy.misc
except Exception:
    pass

import gym
from gym.spaces import Box, Discrete

from carla_env.rewards import compute_reward
from carla_env.classifier_converter import KEEP_CLASSIFICATIONS, fuse_with_depth, rgb_depth_image, RECLASSIFIER, DL_RECLASSIFIER
import carla_env.termination as TERM

SERVER_BINARY = os.environ.get("CARLA_SERVER", os.path.expanduser("/usr/share/carla/current/CarlaUE4.sh"))
if not os.path.exists(SERVER_BINARY):
    print("Since no $CARLA_SERVER -> CarlaUE4.sh binary exists, env.config['server_binary'] is no longer optional set.")

# Set this where you want to save image outputs (or empty string to disable)
CARLA_OUT_PATH = os.environ.get("CARLA_OUT", os.path.expanduser("/tmp/carla_out"))
if CARLA_OUT_PATH and not os.path.exists(CARLA_OUT_PATH):
    os.makedirs(CARLA_OUT_PATH)

try:
    from carla.client import CarlaClient
    from carla.sensor import Camera
    from carla.settings import CarlaSettings
except Exception as e:
    print("Failed to import Carla python libs.")
    raise e

# Mapping from string repr to one-hot encoding index to feed to the model
COMMAND_ORDINAL = {
    "REACH_GOAL": 0,
    "GO_STRAIGHT": 1,
    "TURN_RIGHT": 2,
    "TURN_LEFT": 3,
    "LANE_FOLLOW": 4,
}

# Number of retries if the server doesn't respond
RETRIES_ON_ERROR = 5

# Dummy Z coordinate to use when we only care about (x, y)
GROUND_Z = 22

# Default environment configuration
ENV_CONFIG = {
    "server_binary": SERVER_BINARY,
    "carla_out_path": CARLA_OUT_PATH,
    "measurements_subdir": "measurements",
    "save_images_rgb": False,
    "save_images_class": False,
    "save_images_fusion": False,
    "save_image_raw_class": False,
    "save_ds_components": False,
    "save_image_frequency": 1,
    "deterministic_scenario_count": 0,  # Set to >0 if you want to loop through all scenarios X times then stop
    "framestack": 2,
    "convert_images_to_video": True,
    TERM.EARLY_TERMINATIONS: [key for key in TERM.TERMINATION_FUNCTIONS.keys()],
    "verbose": True,
    "reward_function": "lane_keep",
    "render_x_res": 800,
    "render_y_res": 600,
    "x_res": 80,
    "y_res": 80,
    "server_map": "/Game/Maps/Town02",
    "scenarios": scenarios.TOWN2_TRAIN,
    "squash_action_logits": False,
    "server_restart_interval": 50,
    "fps": 50,
    "quality": "Low",
    "segmenter_graph": None,
    "reward_noise": 0.01,
    "control_gamma": 0.85  # This means about 200ms to fully change wheel to new position.
}

ALL_SPEEDS = [-1.0, -0.5, -0.25, 0, 0.25, 0.5, 0.75, 1.0]
ALL_TURNS = [-0.5, -0.25, 0, 0.25, 0.5]
ALL_ACTIONS = [[speed, turn] for speed in ALL_SPEEDS for turn in ALL_TURNS]
DISCRETE_ACTIONS = {i: ALL_ACTIONS[i] for i in range(len(ALL_ACTIONS))}

# Number of classifications from the pixel parser
EXTRA_FRAMES = 2
FRAME_DEPTH = KEEP_CLASSIFICATIONS + EXTRA_FRAMES


live_carla_processes = set()


def cleanup():
    print("Killing live carla processes", live_carla_processes)
    for pgid in live_carla_processes:
        os.killpg(pgid, signal.SIGKILL)


atexit.register(cleanup)


def prettify_depth(depth):
    return 1 - np.sqrt(np.sqrt(np.sqrt(depth)))
    # return 1 - np.sqrt(depth)
    # return 1 - depth

class CarlaEnv(gym.Env):

    def __init__(self, config=ENV_CONFIG):
        self.config = config
        self.city = self.config["server_map"].split("/")[-1]

        self.action_space = Discrete(len(DISCRETE_ACTIONS))

        # RGB Camera
        self.frame_shape = (config["y_res"], config["x_res"])
        self.frame_depth = FRAME_DEPTH
        if not config["use_dsf"]:
            self.frame_depth = 3  # depth, segmentation, and speed
        image_space = Box(
            0, 1, shape=(
                config["y_res"], config["x_res"],
                self.frame_depth * config["framestack"]), dtype=np.float32)
        self.observation_space = image_space

        # Create the deeplab model, if provided
        self.sensor_style = self.config["sensor_style"]
        self.deeplab_model = None
        if self.config["deeplab"] is not None:
            self.deeplab_model = self.config["deeplab"]

        # TODO(ekl) this isn't really a proper gym spec
        self._spec = lambda: None
        self._spec.id = "Carla-v0"

        self.server_port = None
        self.server_process = None
        self.client = None
        self.num_steps = 0
        self.total_reward = 0
        self.prev_measurement = None
        self.prev_image = None
        self.episode_id = None
        self.measurements_file = None
        self.weather = None
        self.scenario = None
        self.start_pos = None
        self.start_coord = None
        self.last_obs = None
        self.last_full_obs = None
        self.framestack = [None] * config["framestack"]
        self.framestack_index = 0
        self.running_restarts = 0
        self.on_step = None
        self.on_next = None
        self.photo_index = 1
        self.episode_index = 0
        self.last_action = np.array([0.0, 0.0])
        self.remaining_deterministic_scenarios = config["deterministic_scenario_count"]
        self.scenario_index = 0
        self.weather_index = 0

    def init_server(self):
        print("Initializing new Carla server...")
        # Create a new server process and start the client.
        self.server_port = random.randint(10000, 60000)
        self.server_process = subprocess.Popen(
            [self.config["server_binary"], self.config["server_map"],
             "-windowed",
             "-ResX={}".format(self.config["render_x_res"]),
             "-ResY={}".format(self.config["render_y_res"]),
             # "-ResX=512",
             # "-ResY=512",
             "-fps={}".format(self.config["fps"]),
             "-carla-server",
             "-carla-world-port={}".format(self.server_port)],
            preexec_fn=os.setsid, stdout=open(os.devnull, "w"))
        live_carla_processes.add(os.getpgid(self.server_process.pid))

        for i in range(RETRIES_ON_ERROR):
            try:
                self.client = CarlaClient("localhost", self.server_port)
                return self.client.connect()
            except Exception as e:
                print("Error connecting: {}, attempt {}".format(e, i))
                time.sleep(2)

    def clear_server_state(self):
        print("Clearing Carla server state")
        try:
            if self.client:
                self.client.disconnect()
                self.client = None
        except Exception as e:
            print("Error disconnecting client: {}".format(e))
            pass
        if self.server_process:
            pgid = os.getpgid(self.server_process.pid)
            os.killpg(pgid, signal.SIGKILL)
            live_carla_processes.remove(pgid)
            self.server_port = None
            self.server_process = None

    def __del__(self):
        self.clear_server_state()

    def reset(self):
        if self.on_next is not None:
            self.on_next()

        error = None
        self.running_restarts += 1
        for _ in range(RETRIES_ON_ERROR):
            try:
                # Force a full reset of Carla after some number of restarts
                if self.running_restarts > self.config["server_restart_interval"]:
                    print("Shutting down carla server...")
                    self.running_restarts = 0
                    self.clear_server_state()

                # If server down, initialize
                if not self.server_process:
                    self.init_server()

                # Run episode reset
                return self._reset()
            except Exception as e:
                print("Error during reset: {}".format(traceback.format_exc()))
                self.clear_server_state()
                error = e
                time.sleep(5)
        raise error

    def _build_camera(self, name, post):
        camera = Camera(name, PostProcessing=post)
        camera.set_image_size(
            self.config["render_x_res"], self.config["render_y_res"])
        camera.set_position(x=2.4, y=0, z=0.8)
        camera.set_rotation(pitch=-40, roll=0, yaw=0)
        # camera.FOV = 100
        return camera


    def _next_scenario(self):
        scenarios = self.config["scenarios"]

        # Random
        if self.remaining_deterministic_scenarios == 0:
            self.scenario = random.choice(scenarios)
            assert self.scenario["city"] == self.city, (self.scenario, self.city)
            self.weather = random.choice(self.scenario["weather_distribution"])

        # Deterministic
        else:
            # Done?
            if self.scenario_index >= len(scenarios):
                raise DoneError()

            # Get the scenario and weather
            self.scenario = scenarios[self.scenario_index]
            assert self.scenario["city"] == self.city, (self.scenario, self.city)
            weathers = self.scenario["weather_distribution"]
            self.weather = weathers[self.weather_index]

            # Increment weather - go to next scenario if needed
            self.weather_index += 1
            if self.weather_index >= len(weathers):
                self.weather_index = 0
                self.scenario_index += 1


    def _reset(self):
        self.num_steps = 0
        self.total_reward = 0
        self.episode_index += 1
        self.photo_index = 1
        self.prev_measurement = None
        self.prev_image = None
        self.episode_id = datetime.today().strftime("%Y-%m-%d_%H-%M-%S_%f")
        self.measurements_file = None
        self.last_action = np.array([0.0, 0.0])

        # Create a CarlaSettings object. This object is a wrapper around
        # the CarlaSettings.ini file. Here we set the configuration we
        # want for the new episode.
        settings = CarlaSettings()
        self._next_scenario()
        settings.set(
            SynchronousMode=True,
            SendNonPlayerAgentsInfo=False,
            NumberOfVehicles=self.scenario["num_vehicles"],
            NumberOfPedestrians=self.scenario["num_pedestrians"],
            WeatherId=self.weather,
            QualityLevel=self.config["quality"])
        settings.randomize_seeds()

        # Add the cameras
        if self.config["save_images_rgb"] or self.sensor_style != "GROUND_TRUTH":
            settings.add_sensor(self._build_camera(name="CameraRGB", post="SceneFinal"))
        settings.add_sensor(self._build_camera(name="CameraDepth", post="Depth"))
        if self.config["save_images_class"] or self.sensor_style != "DEEPLAB":
            settings.add_sensor(self._build_camera(name="CameraClass", post="SemanticSegmentation"))

        # Setup start and end positions
        scene = self.client.load_settings(settings)
        positions = scene.player_start_spots
        self.start_pos = positions[self.scenario["start_pos_id"]]
        self.start_coord = [
            self.start_pos.location.x // 100, self.start_pos.location.y // 100]
        print("Start pos {} ({})".format(self.scenario["start_pos_id"], self.start_coord))

        # Notify the server that we want to start the episode at the
        # player_start index. This function blocks until the server is ready
        # to start the episode.
        print("Starting new episode...")
        self.client.start_episode(self.scenario["start_pos_id"])

        observation, py_measurements = self._read_observation(np.array([0.0, 0.0]))
        py_measurements["control"] = {
            "throttle_brake": 0,
            "steer": 0,
            "throttle": 0,
            "brake": 0,
            "reverse": False,
            "hand_brake": False,
        }
        self.prev_measurement = py_measurements
        return observation

    def process_signal_deeplab(self):
        if self.sensor_style == "GROUND_TRUTH":
            return False
        if self.sensor_style == "DEEPLAB":
            return True
        if self.sensor_style == "RANDOM":
            return random.choice([True, False])
        raise Exception("Unsupported sensor style.")

    def framestack_obs(self, obs):
        new_obs = obs
        num_frames = int(self.config["framestack"])
        if num_frames > 1:
            # Spread out to number of frames
            frame_array = [obs] * num_frames
            for frame_index in range(1, num_frames):
                index = (self.framestack_index - frame_index) % num_frames
                if self.framestack[index] is not None:
                    frame_array[frame_index] = self.framestack[index]

            # Concatenate into a single np array
            new_obs = np.concatenate(frame_array, axis=2)

        # Store frame
        self.framestack[self.framestack_index] = obs
        self.framestack_index = (self.framestack_index + 1) % num_frames
        self.last_obs = obs

        # Return
        return new_obs

    def step(self, action, explore=0.0):
        try:
            obs = self._step(action, explore=explore)
            self.last_full_obs = obs
            return obs
        except Exception:
            print(
                "Error during step, terminating episode early",
                traceback.format_exc())
            self.clear_server_state()
            return (self.last_full_obs, 0.0, True, {})


    def _convert_action_to_steering(self, action_index):
        # Determine the discrete action
        action = np.array(DISCRETE_ACTIONS[int(action_index)])
        if self.num_steps < self.config["episode_deadzone"]:
            action = np.array([0.0, 0.0])

        # Determine movement amount
        gamma = self.config["control_gamma"]
        action = self.last_action * gamma + action * (1 - gamma)
        self.last_action = action

        # Values
        raw_throttle_brake = np.asscalar(action[0])
        raw_steering = np.asscalar(action[1])

        # Convert into proper controls
        assert len(action) == 2, "Invalid action {}".format(action)
        if self.config["squash_action_logits"]:
            forward = 2 * float(sigmoid(raw_throttle_brake) - 0.5)
            throttle = float(np.clip(forward, 0, 1))
            brake = float(np.abs(np.clip(forward, -1, 0)))
            steer = 2 * float(sigmoid(raw_steering) - 0.5)
        else:
            throttle = float(np.clip(raw_throttle_brake, 0, 1))
            brake = float(np.abs(np.clip(raw_throttle_brake, -1, 0)))
            steer = float(np.clip(raw_steering, -1, 1))
        reverse = False
        hand_brake = False

        return action, {
            "throttle_brake": raw_throttle_brake,
            "steer": steer,
            "throttle": throttle,
            "brake": brake,
            "reverse": reverse,
            "hand_brake": hand_brake,
        }

    def _send_control_to_server(self, control):
        if self.config["verbose"]:
            print(
                "steer", control["steer"],
                "throttle", control["throttle"],
                "brake", control["brake"],
                "reverse", control["reverse"])

        self.client.send_control(
            steer=control["steer"],
            throttle=control["throttle"],
            brake=control["brake"],
            hand_brake=control["hand_brake"],
            reverse=control["reverse"])


    def _step(self, action_index, explore=0.0):
        # Determine action and send it to server
        action, control = self._convert_action_to_steering(action_index)
        self._send_control_to_server(control)

        # Process observations
        observation, py_measurements = self._read_observation(action)
        if self.config["verbose"]:
            print("Next command", py_measurements["next_command"])
        if type(action) is np.ndarray:
            py_measurements["action"] = [float(a) for a in action]
        else:
            py_measurements["action"] = action
        py_measurements["control"] = control
        py_measurements["explore"] = explore

        # Done?
        finished = self.num_steps > self.scenario["max_steps"]
        failed = TERM.compute_termination(
            curr=py_measurements,
            prev=self.prev_measurement,
            terminations=self.config[TERM.EARLY_TERMINATIONS])
        done = finished or failed
        py_measurements["finished"] = finished
        py_measurements["failed"] = failed
        py_measurements["done"] = done

        # Determine Reward
        reward = compute_reward(self, self.prev_measurement, py_measurements, deadzone=self.config["episode_deadzone"])
        self.total_reward += reward
        py_measurements["reward"] = reward
        py_measurements["total_reward"] = self.total_reward
        self.prev_measurement = py_measurements

        # Callback
        if self.on_step is not None:
            self.on_step(py_measurements)

        # Write out measurements to file
        if self.config["carla_out_path"]:
            # Ensure measurements dir exists
            measurements_dir = os.path.join(self.config["carla_out_path"], self.config["measurements_subdir"])
            if not os.path.exists(measurements_dir):
                os.mkdir(measurements_dir)

            if not self.measurements_file:
                self.measurements_file = open(
                    os.path.join(
                        measurements_dir,
                        "m_{}.json".format(self.episode_id)),
                    "w")
            self.measurements_file.write(json.dumps(py_measurements))
            self.measurements_file.write("\n")
            if done:
                self.measurements_file.close()
                self.measurements_file = None
                # if self.config["convert_images_to_video"]:
                #     self.images_to_video(camera_name="RGB")
                #     self.images_to_video(camera_name="Depth")
                #     self.images_to_video(camera_name="Class")

        self.num_steps += 1
        return observation, reward, done, py_measurements

    def _fuse_observations(self, sensor_data, speed, action):
        # Convert the base input
        depth = sensor_data['CameraDepth'].data
        depth = depth.astype(np.float32)

        # Convert image to class data or grab ground truth?
        if self.process_signal_deeplab():
            img = sensor_data['CameraRGB'].data
            deeplab_clazz = self.deeplab_model.run(img)
            clazz = DL_RECLASSIFIER(deeplab_clazz)
        else:
            clazz = RECLASSIFIER(sensor_data['CameraClass'].data)

        # Fuse with depth!
        if self.config["use_dsf"]:
            obs = fuse_with_depth(clazz, depth, extra_layers=EXTRA_FRAMES)
        else:
            shape = clazz.shape
            obs = np.full((shape[0], shape[1], self.frame_depth), 1, dtype=np.float32)
            obs[:, :, 0] = depth

        # Fuse with normalized classes
        obs[:, :, self.frame_depth - 2] = clazz / KEEP_CLASSIFICATIONS

        # Fuse with speed (normalized to 1  = desired speed)
        obs[:, :, self.frame_depth - 1] = speed / 25.0

        return obs, clazz


    def _read_observation(self, action):
        # Read the data produced by the server this frame.
        measurements, sensor_data = self.client.read_data()
        cur = measurements.player_measurements

        # Print some of the measurements.
        if self.config["verbose"]:
            print_measurements(measurements)

        # Fuse the observation data to create a single observation
        observation, clazzes = self._fuse_observations(sensor_data, cur.forward_speed, action)

        py_measurements = {
            "episode_id": self.episode_id,
            "step": self.num_steps,
            "x": cur.transform.location.x,
            "y": cur.transform.location.y,
            "x_orient": cur.transform.orientation.x,
            "y_orient": cur.transform.orientation.y,
            "forward_speed": cur.forward_speed,
            "collision_vehicles": cur.collision_vehicles,
            "collision_pedestrians": cur.collision_pedestrians,
            "collision_other": cur.collision_other,
            "intersection_offroad": cur.intersection_offroad,
            "intersection_otherlane": cur.intersection_otherlane,
            "weather": self.weather,
            "map": self.config["server_map"],
            "start_coord": self.start_coord,
            "current_scenario": self.scenario,
            "x_res": self.config["x_res"],
            "y_res": self.config["y_res"],
            "num_vehicles": self.scenario["num_vehicles"],
            "num_pedestrians": self.scenario["num_pedestrians"],
            "max_steps": self.scenario["max_steps"],
            "applied_penalty": False,
            "total_reward": self.total_reward,
        }

        if self.config["carla_out_path"] \
                and self.num_steps % self.config["save_image_frequency"] == 0\
                and self.num_steps > self.config["episode_deadzone"]:
            self.take_photo(
                sensor_data=sensor_data,
                depth_data=sensor_data["CameraDepth"],
                raw_class_data=sensor_data["CameraClass"],
                class_data=clazzes,
                fused_data=observation
            )

        assert observation is not None, sensor_data
        stacked_observation = self.framestack_obs(observation)
        return stacked_observation, py_measurements

    def images_dir(self):
        dir = os.path.join(self.config["carla_out_path"], "images")
        if not os.path.exists(dir):
            os.mkdir(dir)
        return dir

    def episode_dir(self):
        episode_dir = os.path.join(self.images_dir(), "episode_" + str(self.episode_index))
        if not os.path.exists(episode_dir):
            os.mkdir(episode_dir)
        return episode_dir

    def take_photo(self, sensor_data, depth_data, raw_class_data, class_data, fused_data):
        # Get the proper locations\
        episode_dir = self.episode_dir()
        photo_index = self.photo_index
        name_prefix = "episode_{:>04}_step_{:>04}".format(self.episode_index, photo_index)
        self.photo_index += 1

        # Save the image
        if self.config["save_images_rgb"]:
            rgb_image = sensor_data["CameraRGB"]
            image_path = os.path.join(episode_dir, name_prefix + "_rgb.png")
            scipy.misc.imsave(image_path, rgb_image.data)

        # Save the classes
        if self.config["save_images_class"]:
            depth_path = os.path.join(episode_dir, name_prefix + "_depth.png")
            scipy.misc.imsave(depth_path, prettify_depth(depth_data.data))

            classes_path = os.path.join(episode_dir, name_prefix + "_class.png")
            scipy.misc.imsave(classes_path, class_data.data)

        if self.config["save_image_raw_class"]:
            raw_converted = RECLASSIFIER(raw_class_data.data)
            classes_path = os.path.join(episode_dir, name_prefix + "_raw_class.png")
            scipy.misc.imsave(classes_path, raw_converted)

        if self.config["save_ds_components"]:
            ds_1 = os.path.join(episode_dir, name_prefix + "_ds1.png")
            scipy.misc.imsave(ds_1, prettify_depth(fused_data[:, :, 0]))
            ds_2 = os.path.join(episode_dir, name_prefix + "_ds2.png")
            scipy.misc.imsave(ds_2, prettify_depth(fused_data[:, :, 1]))
            ds_3 = os.path.join(episode_dir, name_prefix + "_ds3.png")
            scipy.misc.imsave(ds_3, prettify_depth(fused_data[:, :, 2]))

        # Save the class images
        if self.config["save_images_fusion"]:
            fused_images = rgb_depth_image(class_data, depth_data.data)
            fused_path = os.path.join(episode_dir, name_prefix + "_fused.png")
            scipy.misc.imsave(fused_path, fused_images.data)

    def images_to_video(self, camera_name):
        # Video directory
        videos_dir = os.path.join(self.config["carla_out_path"], "Videos" + camera_name)
        if not os.path.exists(videos_dir):
            os.makedirs(videos_dir)

        # Build command
        print("FPS might not be properly implemented")
        video_fps = self.config["fps"] / self.config["saveimage_frequency"]
        ffmpeg_cmd = (
            "ffmpeg -loglevel -8 -r 10 -f image2 -s {x_res}x{y_res} "
            "-start_number 0 -i "
            "{img}_%04d.jpg -vcodec libx264 {vid}.mp4 && rm -f {img}_*.jpg "
        ).format(
            x_res=self.config["render_x_res"],
            y_res=self.config["render_y_res"],
            vid=os.path.join(videos_dir, self.episode_id),
            img=os.path.join(self.config["carla_out_path"], "Camera" + camera_name, self.episode_id))

        # Execute command
        print("Executing ffmpeg command: ", ffmpeg_cmd)
        try:
            subprocess.call(ffmpeg_cmd, shell=True, timeout=60)
        except Exception as ex:
            print("FFMPEG EXPIRED")
            print(ex)


def print_measurements(measurements):
    number_of_agents = len(measurements.non_player_agents)
    player_measurements = measurements.player_measurements
    message = "Vehicle at ({pos_x:.1f}, {pos_y:.1f}), "
    message += "{speed:.2f} km/h, "
    message += "Collision: {{vehicles={col_cars:.0f}, "
    message += "pedestrians={col_ped:.0f}, other={col_other:.0f}}}, "
    message += "{other_lane:.0f}% other lane, {offroad:.0f}% off-road, "
    message += "({agents_num:d} non-player agents in the scene)"
    message = message.format(
        pos_x=player_measurements.transform.location.x / 100,  # cm -> m
        pos_y=player_measurements.transform.location.y / 100,
        speed=player_measurements.forward_speed,
        col_cars=player_measurements.collision_vehicles,
        col_ped=player_measurements.collision_pedestrians,
        col_other=player_measurements.collision_other,
        other_lane=100 * player_measurements.intersection_otherlane,
        offroad=100 * player_measurements.intersection_offroad,
        agents_num=number_of_agents)
    print(message)


def sigmoid(x):
    x = float(x)
    return np.exp(x) / (1 + np.exp(x))


if __name__ == "__main__":
    for _ in range(2):
        env = CarlaEnv()
        obs = env.reset()
        print("reset", obs)
        start = time.time()
        done = False
        i = 0
        total_reward = 0.0
        while not done:
            i += 1
            obs, reward, done, info = env.step(1)
            total_reward += reward
            print(i, "rew", reward, "total", total_reward, "done", done)
        print("{} fps".format(100 / (time.time() - start)))
