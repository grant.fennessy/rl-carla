import random

DEBUGGER = True
DESIRED_SPEED = 25
SPEED_CONVERSION = 3.6


# Returns a 0.0-1.0 ratio representing how close we are to the desired speed.
# If at 1.0, then at desired speed.  Anything else is above or below.  Negative
# speeds are capped at 0.0.
def desired_speed_ratio(speed):
    speed = speed * SPEED_CONVERSION
    ratio = speed / DESIRED_SPEED
    # Once at 1, start going back down
    ratio = 1 - abs(1 - ratio)
    # Bind between 0 and 1
    return max(0.0, min(1.0, ratio))


# Each step should provide a penalty based on how far away we are from the desired speed
def desired_speed_reward(prev, current):
    return desired_speed_ratio(current["forward_speed"])


def delta_speed_reward(prev, current):
    curr_speed = current["forward_speed"] * SPEED_CONVERSION
    prev_speed = prev["forward_speed"] * SPEED_CONVERSION
    delta = min(1.0, abs(curr_speed - prev_speed))
    if prev_speed < DESIRED_SPEED and prev_speed <= curr_speed:
        return delta
    if prev_speed > DESIRED_SPEED and prev_speed >= curr_speed:
        return delta
    return -delta

# Applies a penalty if the vehicle is becoming MORE out of lane/road than it was previously.
def moving_out_of_lane(prev, current):
    # If at all out of lane... no reward!
    current_lane = current["intersection_offroad"] + current["intersection_otherlane"]
    prev_lane = prev["intersection_offroad"] + prev["intersection_otherlane"]
    return current_lane > prev_lane

# Applies a penalty if the vehicle is becoming MORE out of lane/road than it was previously.
def within_lane_reward(prev, current):
    # If at all out of lane... no reward!
    if current["intersection_offroad"] > 0 or current["intersection_otherlane"] > 0:
        return 0

    # Staying! Reward is a function of speed
    return 1.0 * desired_speed_ratio(current["forward_speed"])



























def speed(prev, current):
    # Provide a speed reward IF FULLY IN LANE ONLY
    # Reward peaks at desired speed, becoming negative if speed too high
    speed = current["forward_speed"] * SPEED_CONVERSION
    if current["intersection_offroad"] == 0 and current["intersection_otherlane"] == 0:
        # When at 0, receive very slight negative reward
        if speed < 1:
            return -(1 - speed) * 0.1
        return 1 - abs((speed - DESIRED_SPEED) / DESIRED_SPEED)

    # Push a penalty if exiting road for first time
    elif prev["intersection_offroad"] == 0 and prev["intersection_otherlane"] == 0:
        return -0.1
    else:
        return 0.0


def intersection_offroad(prev, current):
    offroad = current["intersection_offroad"]
    if offroad > 0:
        return -0.5
    return 0.0

def intersection_otherlane(prev, current):
    otherlane = current["intersection_otherlane"]
    if otherlane > 0:
        return -0.3
    return 0.0

def collision(prev, current):
    curr_collision = current["collision_other"] + current["collision_pedestrians"] + current["collision_vehicles"]
    prev_collision = prev["collision_other"] + prev["collision_pedestrians"] + prev["collision_vehicles"]
    if curr_collision > prev_collision:
        return -100
    return 0.0

# Applies a penalty to dramatic changes in steering
def steer_delta_penalty(prev, current):
    steer_delta = abs(current["control"]["steer"] - prev["control"]["steer"])
    return -(steer_delta ** 2) / 10

# Applies a penalty to dramatic changes in throttle
def throttle_delta_penalty(prev, current):
    throttle_delta = abs(current["control"]["throttle_brake"] - prev["control"]["throttle_brake"])
    return -(throttle_delta ** 2) / 50

def compile_rewards_level1(prev, current):
    # Determine the list of rewards
    rewards = dict()

    rewards["int_offroad"] = intersection_offroad(prev, current)
    rewards["int_other"] = intersection_otherlane(prev, current)
    rewards["speed"] = speed(prev, current)
    rewards["collision"] = collision(prev, current)
    #rewards["steer"] = steer_delta_penalty(prev, current)
    #rewards["throttle"] = throttle_delta_penalty(prev, current)
    return rewards


def compile_rewards_level2(prev, current):
    # Determine the list of rewards
    rewards = dict()
    return rewards


def inlane_percent(current):
    offlane_p = min(1.0, current["intersection_offroad"] + current["intersection_otherlane"])
    return round((1.0 - offlane_p) * 100.0)


def compute_rewards(env, prev, current, rewards_func, deadzone=0.0):
    # Get a the dict of penalties
    rewards = rewards_func(prev, current)
    current["rewards"] = rewards
    reward = 0.0
    for reward_key in rewards:
        reward += rewards[reward_key]

    if DEBUGGER:
        reward_list = list()
        for reward_key in rewards:
            reward_value = rewards[reward_key]
            reward_list.append("{key}: {value:+07.4f}".format(key=reward_key, value=reward_value))

        # Only print when over deadzone
        if current["step"] > deadzone:
            print(",  ".join(reward_list) +
                  " || Step Reward: {:+07.4f}".format(reward) +
                  " Cumulative: {:+08.2f}".format(prev["total_reward"] + reward) +
                  "    ==> Speed: {:+05.2f} km/hr".format(current["forward_speed"] * SPEED_CONVERSION) +
                  ", Inlane: {:>03d}%".format(inlane_percent(current)) +
                  ", Throttle: {:+05.2f}".format(current["control"]["throttle_brake"]) +
                  ", Steer: {:+05.2f}".format(current["control"]["steer"]) +
                  ", Explore: {:0.2f}".format(current["explore"])
            )

    # Finally we want to apply a bit of noise to try and prevent strange overfitting events
    if env.config["reward_noise"] > 0.0:
        reward += env.config["reward_noise"] * (random.random() * 2 - 1)
    return reward


def reward_train_1(env, prev, current, deadzone=0.0):
    return compute_rewards(env, prev, current, compile_rewards_level1, deadzone=deadzone)

def reward_train_2(env, prev, current, deadzone=0.0):
    return compute_rewards(env, prev, current, compile_rewards_level2, deadzone=deadzone)


REWARD_FUNCTIONS = {
    "train_1": reward_train_1,
    "train_2": reward_train_2,
}


def compute_reward(env, prev, current, deadzone=0.0):
    return REWARD_FUNCTIONS[env.config["reward_function"]](
        env, prev, current, deadzone=deadzone)
