from carla_env.env import CarlaEnv
from model_builder import model_function
import deepq_learner
from spec_files import FILENAMES, ENV_LEVELS, DEEPQ_BASE


def run_model_at_level(level):
    # Print
    print("Running model at level: " + str(level))

    # Build env
    config = ENV_LEVELS["train_{}".format(level)]
    # use_deeplab(config)
    # use_deeplab_mixed(config)
    env = CarlaEnv(config)

    # Learn
    learn = deepq_learner.DeepqLearner(
        env=env,
        q_func=model_function(is_training=True),
        config=DEEPQ_BASE["train_{}".format(level)]
    )
    learn.run()

    env.close()

    # Save the file
    learn.checkpoint()
    learn.save(FILENAMES["model"])


if __name__ == '__main__':
    # run_model_at_level(1)
    # run_model_at_level(2)
    run_model_at_level(3)
